The Accord Project - Copyright 2009-2010 Neociclo Informatica Ltda., and is
licensed under the GNU Affero General Public License version 3.0 as published by
the Free Software Foundation.

A summary of the individual contributors is given below. Any omission should be
sent to Rafael Marins <rafael.marins@neociclo.com>.

SVN Login(s)            Name
-------------------------------------------------------------------------------
brunoborges             Bruno Borges
rmarins                 Rafael Marins
-------------------------------------------------------------------------------

* Neociclo is a registered trademark of Neociclo Informatica Ltda.
