<?xml version="1.0" encoding="UTF-8"?>
<!--
    Neociclo Accord - Open Source B2B Integration Suite
    Copyright (C) 2005-2010 Neociclo, http://www.neociclo.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses />.

    $Id: pom.xml 313 2010-05-07 15:37:28Z rmarins $
-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

  <parent>
    <groupId>org.neociclo.accord</groupId>
    <artifactId>accord-parent</artifactId>
    <version>1.8</version>
  </parent>

  <modelVersion>4.0.0</modelVersion>
  <groupId>org.neociclo.accord.odetteftp</groupId>
  <artifactId>odetteftp-parent</artifactId>
  <name>Accord Odette FTP</name>
  <version>1.2.0-SNAPSHOT</version>
  <packaging>pom</packaging>

  <description>
      Accord Odette FTP Library is a Java API for developing OFTP2-enabled applications, supporting OFTP 1.3, 1.4 and 2.0 protocol versions.
  </description>

  <licenses>
    <license>
      <name>GNU AFFERO GENERAL PUBLIC LICENSE v3</name>
      <url>http://www.gnu.org/licenses/agpl-3.0.txt</url>
    </license>
  </licenses>

  <distributionManagement>
    <site>
      <id>ow2-repo</id>
      <url>scp://forge.objectweb.org/var/lib/gforge/chroot/home/groups/accord/htdocs/odetteftp</url>
    </site>
  </distributionManagement>

  <developers>
    <developer>
      <id>rmarins</id>
      <name>Rafael Marins</name>
      <organization>Neociclo</organization>
      <timezone>BRST</timezone>
    </developer>
  </developers>

  <scm>
    <connection>scm:svn:svn://svn.forge.objectweb.org/svnroot/accord/odetteftp/trunk</connection>
    <developerConnection>scm:svn:svn+ssh://svn.forge.objectweb.org/svnroot/accord/odetteftp/trunk</developerConnection>
    <url>http://websvn.ow2.org/listing.php?repname=accord&amp;path=/odetteftp/trunk/</url>
  </scm>

  <build>

    <resources>
      <resource>
        <directory>src/main/java</directory>
        <includes>
          <include>**/*.properties</include>
        </includes>
      </resource>
      <resource>
        <directory>src/main/resources</directory>
        <includes>
          <include>**</include>
        </includes>
      </resource>

      <!--
          | example additional resource entries, useful when building Eclipse RCP applications
          -->
      <resource>
        <directory>.</directory>
        <includes>
          <include>plugin.xml</include>
          <include>plugin.properties</include>
          <!-- <include>icons/**</include> -->
        </includes>
      </resource>
    </resources>

    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-surefire-plugin</artifactId>
          <configuration>
            <useFile>false</useFile>
          </configuration>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-site-plugin</artifactId>
          <configuration>
            <siteDirectory>site</siteDirectory>
          </configuration>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-eclipse-plugin</artifactId>
          <configuration>
            <addVersionToProjectName>true</addVersionToProjectName>
          </configuration>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-antrun-plugin</artifactId>
          <version>${antrun.version}</version>
        </plugin>
      </plugins>
    </pluginManagement>

    <plugins>
      <plugin>
        <groupId>org.ops4j</groupId>
        <artifactId>maven-pax-plugin</artifactId>
        <version>${pax.version}</version>
        <!--
           | enable improved OSGi compilation support for the bundle life-cycle.
           | to switch back to the standard bundle life-cycle, move this setting
           | down to the maven-bundle-plugin section
          -->
        <extensions>true</extensions>
      </plugin>
      <plugin>
        <groupId>org.apache.felix</groupId>
        <artifactId>maven-bundle-plugin</artifactId>
        <version>${felix.version}</version>
        <extensions>true</extensions>
        <configuration>
          <excludeDependencies>${odetteftp.osgi.exclude.dependencies}</excludeDependencies>
          <instructions>
            <Bundle-Name>${project.artifactId}</Bundle-Name>
            <Bundle-SymbolicName>${odetteftp.osgi.symbolic.name}</Bundle-SymbolicName>
            <Bundle-Activator>${odetteftp.osgi.activator}</Bundle-Activator>
            <Export-Package>${odetteftp.osgi.export}</Export-Package>
            <Import-Package>${odetteftp.osgi.import}</Import-Package>
            <DynamicImport-Package>${odetteftp.osgi.dynamic}</DynamicImport-Package>
            <Private-Package>${odetteftp.osgi.private.pkg}</Private-Package>
            <Implementation-Title>Accord OFTP2 Library</Implementation-Title>
            <Implementation-Version>${project.version}</Implementation-Version>
            <_versionpolicy>${odetteftp.osgi.import.default.version}</_versionpolicy>
            <_failok>${odetteftp.osgi.failok}</_failok>
          </instructions>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-antrun-plugin</artifactId>
        <inherited>true</inherited>
        <executions>
          <execution>
            <id>create-prop</id>
            <phase>initialize</phase>
            <configuration>
              <tasks>
                <taskdef resource="net/sf/antcontrib/antcontrib.properties" classpathref="maven.plugin.classpath" />
                <property name="ant.regexp.regexpimpl" value="org.apache.tools.ant.util.regexp.Jdk14RegexpRegexp" />
                <property name="mv" value="${project.version}" />
                <echo message="Maven version: ${mv}" />
                <propertyregex property="ov.p1" input="${mv}" regexp="(\d+)(?:\.(\d+)(?:\.(\d+))?)?(?:[^a-zA-Z0-9](.*))?" replace="\1" defaultValue="0" />
                <propertyregex property="ov.p2" input="${mv}" regexp="(\d+)(?:\.(\d+)(?:\.(\d+))?)?(?:[^a-zA-Z0-9](.*))?" replace=".\2" defaultValue=".0" />
                <propertyregex property="ov.p3" input="${mv}" regexp="(\d+)(?:\.(\d+)(?:\.(\d+))?)?(?:[^a-zA-Z0-9](.*))?" replace=".\3" defaultValue=".0" />
                <propertyregex property="ov.p4" input="${mv}" regexp="(\d+)(?:\.(\d+)(?:\.(\d+))?)?(?:[^a-zA-Z0-9](.*))?" replace=".\4" defaultValue="" />
                <propertyregex property="ov.p1a" input="${ov.p1}" regexp="(.+)" replace="\1" defaultValue="0" />
                <propertyregex property="ov.p2a" input="${ov.p2}" regexp="(\..+)" replace="\1" defaultValue=".0" />
                <propertyregex property="ov.p3a" input="${ov.p3}" regexp="(\..+)" replace="\1" defaultValue=".0" />
                <propertyregex property="ov.p4a" input="${ov.p4}" regexp="(\..+)" replace="\1" defaultValue="" />
                <property name="ov" value="${ov.p1a}${ov.p2a}${ov.p3a}${ov.p4a}" />
                <property name="os" value="${ov.p1a}${ov.p2a}" />
                <echo message="OSGi version: ${ov}" />
                <mkdir dir="target" />
                <echo file="target/odetteftp.osgi.version.txt">
                  odetteftp.osgi.version.clean = ${ov}
                  odetteftp.schema.version = ${os}
                              </echo>
              </tasks>
            </configuration>
            <goals>
              <goal>run</goal>
            </goals>
          </execution>
        </executions>
        <dependencies>
          <dependency>
            <groupId>javax.xml.bind</groupId>
            <artifactId>jaxb-api</artifactId>
            <version>${jaxb-api-version}</version>
          </dependency>
          <dependency>
            <groupId>com.sun.xml.bind</groupId>
            <artifactId>jaxb-impl</artifactId>
            <version>${jaxb-version}</version>
          </dependency>
          <dependency>
            <groupId>com.sun.xml.bind</groupId>
            <artifactId>jaxb-xjc</artifactId>
            <version>${jaxb-version}</version>
          </dependency>
          <dependency>
            <groupId>ant-contrib</groupId>
            <artifactId>ant-contrib</artifactId>
            <version>1.0b3</version>
          </dependency>
          <dependency>
            <groupId>ant</groupId>
            <artifactId>ant-optional</artifactId>
            <version>1.5.3-1</version>
          </dependency>
        </dependencies>
      </plugin>

      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>properties-maven-plugin</artifactId>
        <inherited>true</inherited>
        <executions>
          <execution>
            <phase>initialize</phase>
            <goals>
              <goal>read-project-properties</goal>
            </goals>
            <configuration>
              <files>
                <file>target/odetteftp.osgi.version.txt</file>
              </files>
            </configuration>
          </execution>
        </executions>
      </plugin>

    </plugins>

  </build>

  <dependencies>
      <!--+
        | ========  Logging dependencies  ========
        +-->
      <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-api</artifactId>
        <version>${slf4j.version}</version>
      </dependency>

      <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-log4j12</artifactId>
        <version>${slf4j.version}</version>
        <scope>test</scope>
      </dependency>

      <dependency>
        <groupId>log4j</groupId>
        <artifactId>log4j</artifactId>
        <version>${log4j.version}</version>
        <scope>test</scope>
        <exclusions>
          <exclusion>
            <groupId>com.sun.jdmk</groupId>
            <artifactId>jmxtools</artifactId>
          </exclusion>
          <exclusion>
            <groupId>com.sun.jmx</groupId>
            <artifactId>jmxri</artifactId>
          </exclusion>
          <exclusion>
            <groupId>javax.jms</groupId>
            <artifactId>jms</artifactId>
          </exclusion>
        </exclusions>
      </dependency>

      <!--+
        | ========  Test-only dependencies  ========
        +-->
      <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>${junit.version}</version>
        <scope>test</scope>
      </dependency>

      <dependency>
        <groupId>org.easymock</groupId>
        <artifactId>easymock</artifactId>
        <version>${easymock.version}</version>
        <scope>test</scope>
      </dependency>
  </dependencies>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>org.neociclo.accord.odetteftp</groupId>
        <artifactId>core-oftp</artifactId>
        <type>bundle</type>
        <version>${project.version}</version>
      </dependency>

      <dependency>
        <groupId>org.neociclo.accord.odetteftp</groupId>
        <artifactId>forby-oftp</artifactId>
        <version>${project.version}</version>
      </dependency>

      <dependency>
        <groupId>org.neociclo.accord.odetteftp</groupId>
        <artifactId>camel-oftp</artifactId>
        <type>bundle</type>
        <version>${project.version}</version>
      </dependency>

      <!-- OSGi dependencies -->
      <dependency>
        <groupId>org.osgi</groupId>
        <artifactId>osgi_R4_core</artifactId>
        <version>${osgi.version}</version>
        <scope>provided</scope>
        <optional>true</optional>
      </dependency>
      <dependency>
        <groupId>org.osgi</groupId>
        <artifactId>osgi_R4_compendium</artifactId>
        <version>${osgi.version}</version>
        <scope>provided</scope>
        <optional>true</optional>
      </dependency>
    </dependencies>
  </dependencyManagement>

  <modules>
    <module>core</module>
    <module>camel-oftp</module>
    <module>examples</module>
<!--
    <module>oftp-isdn</module>
    <module>oftp-spring-integration</module>
    <module>forby-oftp</module>
    <module>cli-oftp</module>
-->
    <module>assemblies</module>
  </modules>

  <properties>

    <!-- compile dependencies version -->
    <netty.version>3.3.1.Final</netty.version>
    <bouncycastle.version>1.45</bouncycastle.version>
    <slf4j.version>1.5.11</slf4j.version>

	<!-- test dependencies version -->
    <log4j.version>1.2.16</log4j.version>
    <junit.version>4.10</junit.version>
    <easymock.version>3.0</easymock.version>

    <!-- eclipse plugin need the jaxb in this pom.xml file -->
    <antrun.version>1.2</antrun.version>
    <jaxb-version>2.1.13</jaxb-version>
    <jaxb-api-version>2.1</jaxb-api-version>

    <!-- osgi dependencies & plugins version -->
    <osgi.version>1.0</osgi.version>
    <felix.version>2.1.0</felix.version>
    <pax.version>1.4</pax.version>

    <!-- OSGi bundles properties -->
    <odetteftp.osgi.import.odetteftp.version>version="[$(version;==;${odetteftp.osgi.version.clean}),$(version;=+;${odetteftp.osgi.version.clean}))"</odetteftp.osgi.import.odetteftp.version>
    <odetteftp.osgi.import.strict.version>version="[$(version;===;${odetteftp.osgi.version.clean}),$(version;==+;${odetteftp.osgi.version.clean}))"</odetteftp.osgi.import.strict.version>
    <odetteftp.osgi.import.default.version>[$(version;==;$(@)),$(version;+;$(@)))</odetteftp.osgi.import.default.version>
    <odetteftp.osgi.import.defaults>
      org.slf4j.*;version="[1.5,1.6)",
    </odetteftp.osgi.import.defaults>
    <odetteftp.osgi.import.before.defaults />
    <odetteftp.osgi.import.additional />
    <odetteftp.osgi.import.pkg>
      !${odetteftp.osgi.export.pkg},     
      org.neociclo.odetteftp.*;${odetteftp.osgi.import.odetteftp.version},
      ${odetteftp.osgi.import.before.defaults},
      ${odetteftp.osgi.import.defaults},
      ${odetteftp.osgi.import.additional},
      *
    </odetteftp.osgi.import.pkg>
    <odetteftp.osgi.activator />
    <odetteftp.osgi.failok>false</odetteftp.osgi.failok>
    <odetteftp.osgi.private.pkg>!*</odetteftp.osgi.private.pkg>
    <odetteftp.osgi.export>${odetteftp.osgi.export.pkg};${odetteftp.osgi.version}</odetteftp.osgi.export>
    <odetteftp.osgi.version>version=${project.version}</odetteftp.osgi.version>
    <odetteftp.osgi.split.pkg>-split-package:=first</odetteftp.osgi.split.pkg>
    <odetteftp.osgi.import>${odetteftp.osgi.import.pkg}</odetteftp.osgi.import>
    <odetteftp.osgi.dynamic />
    <odetteftp.osgi.symbolic.name>${project.groupId}.${project.artifactId}</odetteftp.osgi.symbolic.name>
    <odetteftp.osgi.exclude.dependencies>false</odetteftp.osgi.exclude.dependencies>

  </properties>

  <repositories>
    <!-- SpringSource OSGi (and 3rd-party OSGified) releases -->
    <repository>
      <id>com.springsource.repository.bundles.external</id>
      <name>SpringSource Enterprise Bundle Repository - External Bundle Releases</name>
      <url>http://repository.springsource.com/maven/bundles/external</url>
      <snapshots>
        <enabled>false</enabled>
      </snapshots>
    </repository>
    <repository>
      <id>com.springsource.repository.bundles.release</id>
      <name>SpringSource Enterprise Bundle Repository - SpringSource Bundle Releases</name>
      <url>http://repository.springsource.com/maven/bundles/release</url>
      <snapshots>
        <enabled>false</enabled>
      </snapshots>
    </repository>
    <repository>
      <id>com.springsource.repository.bundles.milestone</id>
      <name>SpringSource Enterprise Bundle Repository - SpringSource Milestone Releases</name>
      <url>http://repository.springsource.com/maven/bundles/milestone</url>
      <snapshots>
        <enabled>false</enabled>
      </snapshots>
    </repository>
    <repository>
      <id>spring-release</id>
      <name>Spring Portfolio Release Repository</name>
      <url>http://maven.springframework.org/release</url>
      <snapshots>
        <enabled>false</enabled>
      </snapshots>
    </repository>
  </repositories>

  <profiles>
    <profile>
      <id>on-sunjdk</id>
      <activation>
        <property>
          <name>java.vendor</name>
          <value>Sun Microsystems Inc.</value>
        </property>
      </activation>

      <build>
        <plugins>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-antrun-plugin</artifactId>
            <inherited>true</inherited>
            <dependencies>
              <dependency>
                <groupId>com.sun</groupId>
                <artifactId>tools</artifactId>
                <version>1.5.0</version>
                <scope>system</scope>
                <systemPath>${java.home}/../lib/tools.jar</systemPath>
              </dependency>
            </dependencies>
          </plugin>
        </plugins>
      </build>
    </profile>
  </profiles>

</project>
